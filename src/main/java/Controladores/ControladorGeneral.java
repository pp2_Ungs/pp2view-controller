package Controladores;

import Vistas.VentanaConfiguracionDeNotificaciones;
import configuradorNotificaciones.PersonaServicios;
import pojos.Persona;

public class ControladorGeneral{

	private static ControladorSeleccionDeServicios controladorServicios;
	private static ControladorConfiguracionDeNotificaciones controladorNotificaciones;
	private PersonaServicios modelo;
	
	public ControladorGeneral(PersonaServicios modelo) {
		this.modelo = modelo;
		this.controladorServicios = new ControladorSeleccionDeServicios(modelo);
		this.controladorNotificaciones = new ControladorConfiguracionDeNotificaciones(modelo);
		this.controladorNotificaciones.inicializar();
	}
	
	public static void configurarServicio (Persona p) {
		controladorServicios.inicializar(p);
	}
	
	public static void aceptarConfiguracionServicio () {
		controladorNotificaciones.inicializar();
	}
	
	public static void cancelarConfiguracionServicio() {
		controladorNotificaciones.inicializar();
	}

}
