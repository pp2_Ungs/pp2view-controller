package Controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Vistas.VentanaSeleccionDeServicios;
import configuradorNotificaciones.PersonaServicios;
import pojos.Persona;
import pojos.ServicioNotificacion;
import pojos.TipoInfraccion;

public class ControladorSeleccionDeServicios implements ActionListener {

	private VentanaSeleccionDeServicios ventanaSeleccionDeServicios;
	private PersonaServicios modelo;
	private Persona persona;

	public ControladorSeleccionDeServicios(PersonaServicios modelo) {
		this.ventanaSeleccionDeServicios = new VentanaSeleccionDeServicios();
		this.modelo = modelo;
		this.ventanaSeleccionDeServicios.getBtnAceptar().addActionListener(this);
		this.ventanaSeleccionDeServicios.getComboBoxInfraccionLeve().addActionListener(this);
		this.ventanaSeleccionDeServicios.getComboBoxInfraccionModerada().addActionListener(this);
		this.ventanaSeleccionDeServicios.getComboBoxInfraccionGrave().addActionListener(this);
	}

	public void inicializar(Persona persona) {
		this.ventanaSeleccionDeServicios.setVisible(true);
		this.persona = persona;
		// aca se le pasaria como parametro el nombre que se selecciono en la tabla
		this.ventanaSeleccionDeServicios.setTxtNombrePersona(persona.getApellido() + ", " + persona.getNombre());
		llenarCombos();
	}

	private void llenarCombos() {

		this.ventanaSeleccionDeServicios.getComboBoxInfraccionLeve().removeAllItems();
		this.ventanaSeleccionDeServicios.getComboBoxInfraccionModerada().removeAllItems();
		this.ventanaSeleccionDeServicios.getComboBoxInfraccionGrave().removeAllItems();

		for (ServicioNotificacion servicio : ServicioNotificacion.values()) {
			this.ventanaSeleccionDeServicios.getComboBoxInfraccionLeve().addItem(servicio);
			this.ventanaSeleccionDeServicios.getComboBoxInfraccionModerada().addItem(servicio);
			this.ventanaSeleccionDeServicios.getComboBoxInfraccionGrave().addItem(servicio);
		}

		this.ventanaSeleccionDeServicios.getComboBoxInfraccionLeve().setSelectedItem(
				modelo.getMapServiciosPersonas().get(persona.getIdPersona()).getMap().get(TipoInfraccion.LEVE));
		this.ventanaSeleccionDeServicios.getComboBoxInfraccionModerada().setSelectedItem(
				modelo.getMapServiciosPersonas().get(persona.getIdPersona()).getMap().get(TipoInfraccion.MODERADA));
		this.ventanaSeleccionDeServicios.getComboBoxInfraccionGrave().setSelectedItem(
				modelo.getMapServiciosPersonas().get(persona.getIdPersona()).getMap().get(TipoInfraccion.GRAVE));
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.ventanaSeleccionDeServicios.getBtnAceptar()) {

			// Guardo configuracion de combox
			int idServicioLeve = this.ventanaSeleccionDeServicios.getComboBoxInfraccionLeve().getSelectedIndex();
			int idServicioModerado = this.ventanaSeleccionDeServicios.getComboBoxInfraccionModerada()
					.getSelectedIndex();
			int idServicioGrave = this.ventanaSeleccionDeServicios.getComboBoxInfraccionGrave().getSelectedIndex();

			modelo.getMapServiciosPersonas().get(persona.getIdPersona()).getMap().put(TipoInfraccion.LEVE,
					ServicioNotificacion.values()[idServicioLeve]);
			modelo.getMapServiciosPersonas().get(persona.getIdPersona()).getMap().put(TipoInfraccion.MODERADA,
					ServicioNotificacion.values()[idServicioModerado]);
			modelo.getMapServiciosPersonas().get(persona.getIdPersona()).getMap().put(TipoInfraccion.GRAVE,
					ServicioNotificacion.values()[idServicioGrave]);

			ControladorGeneral.aceptarConfiguracionServicio();
			this.ventanaSeleccionDeServicios.dispose();

		}

	}

}
