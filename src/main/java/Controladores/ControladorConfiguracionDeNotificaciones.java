package Controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import Vistas.VentanaConfiguracionDeNotificaciones;
import configuradorNotificaciones.PersonaServicios;
import json.JsonReaders;
import pojos.Persona;

public class ControladorConfiguracionDeNotificaciones implements ActionListener{

	private VentanaConfiguracionDeNotificaciones ventanaConfiguracionDeNotificaciones;
	private PersonaServicios modelo;
	
	public ControladorConfiguracionDeNotificaciones(PersonaServicios modelo) {
		this.ventanaConfiguracionDeNotificaciones = new VentanaConfiguracionDeNotificaciones();
		this.ventanaConfiguracionDeNotificaciones.getBtnConfigurar().addActionListener(this);
		this.ventanaConfiguracionDeNotificaciones.getBtnCancelar().addActionListener(this);
		this.modelo = modelo;
	}
	
	public void inicializar() {
		this.ventanaConfiguracionDeNotificaciones.setVisible(true);
		llenarTabla();
	}
	
	//llena tabla de personas
	private void llenarTabla() {
		List<Persona> listaPersonas = JsonReaders.readPersonas();
		this.ventanaConfiguracionDeNotificaciones.getModelPersonas().setRowCount(0);
		for (int i = 0; i < listaPersonas.size(); i++) {
			Object[] fila = { listaPersonas.get(i).getNombre(), listaPersonas.get(i).getApellido(), listaPersonas.get(i).getDNI() };
			this.ventanaConfiguracionDeNotificaciones.getModelPersonas().addRow(fila);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == this.ventanaConfiguracionDeNotificaciones.getBtnConfigurar()) {

			int fila_seleccionada = this.ventanaConfiguracionDeNotificaciones.getTablaPersonas().getSelectedRow();
			//Persona seleccionada. Esta info va al contexto ???? 
			Persona personaSeleccionada = JsonReaders.readPersonas().get(fila_seleccionada);
			
			ControladorGeneral.configurarServicio(personaSeleccionada);
			this.ventanaConfiguracionDeNotificaciones.setVisible(false);
		}
		
		else if(e.getSource() == this.ventanaConfiguracionDeNotificaciones.getBtnCancelar()) {
			this.ventanaConfiguracionDeNotificaciones.dispose();
		}		
	}

}
