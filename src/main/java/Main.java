import Controladores.ControladorGeneral;
import configuradorNotificaciones.PersonaServicios;
import json.JsonReaders;
import pojos.Persona;

public class Main {

	public static void main(String[] args) {

		PersonaServicios modelo = new PersonaServicios();

		for(Persona persona: JsonReaders.readPersonas()) {
			modelo.agregarPersona(persona.getIdPersona());
		}
		
		ControladorGeneral appController = new ControladorGeneral(modelo);
	}

}
