package Vistas;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.Component;
import javax.swing.Box;
import java.awt.Font;
import javax.swing.JComboBox;

public class VentanaSeleccionDeServicios extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnAceptar;
	private JComboBox comboBoxInfraccionLeve;
	private JComboBox comboBoxInfraccionModerada;
	private JComboBox comboBoxInfraccionGrave;
	private JLabel lblNombre;
	
	public VentanaSeleccionDeServicios() 
	{
		super();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 390, 298);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 355, 237);
		contentPane.add(panel);
		panel.setLayout(null);
		
		btnAceptar = new JButton("Agregar");
//		btnAgregarCamilla.addActionListener(this.controlador);
		btnAceptar.setBounds(133, 203, 89, 23);
		
		panel.add(btnAceptar);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		horizontalStrut.setBounds(140, 232, 113, -8);
		panel.add(horizontalStrut);
		
		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 48, 61, 14);
		panel.add(lblNombre);
		
		JLabel lblInfraccinLeve = new JLabel("Infracci\u00F3n Leve");
		lblInfraccinLeve.setBounds(10, 83, 106, 14);
		panel.add(lblInfraccinLeve);
		
		JLabel lblInfraccinModerada = new JLabel("Infracci\u00F3n Moderada");
		lblInfraccinModerada.setBounds(10, 118, 120, 14);
		panel.add(lblInfraccinModerada);
		
		JLabel lblInfraccinGrave = new JLabel("Infracci\u00F3n Grave");
		lblInfraccinGrave.setBounds(10, 153, 106, 14);
		panel.add(lblInfraccinGrave);
		
		comboBoxInfraccionLeve = new JComboBox();
		comboBoxInfraccionLeve.setBounds(140, 80, 161, 20);
		panel.add(comboBoxInfraccionLeve);
		
		comboBoxInfraccionModerada = new JComboBox();
		comboBoxInfraccionModerada.setBounds(140, 115, 161, 20);
		panel.add(comboBoxInfraccionModerada);
		
		comboBoxInfraccionGrave = new JComboBox();
		comboBoxInfraccionGrave.setBounds(140, 150, 161, 20);
		panel.add(comboBoxInfraccionGrave);
		
		JLabel lbltitulo = new JLabel("NOTIFICACIONES POR INFRACCI\u00D3N");
		lbltitulo.setBounds(71, 11, 213, 14);
		panel.add(lbltitulo);
		lbltitulo.setFont(new Font("Tahoma", Font.BOLD, 11));
	
	}
	
	public JButton getBtnAceptar() 
	{
		return btnAceptar;
	}
	
	public void setTxtNombrePersona(String nombre) 
	{
		lblNombre.setText(nombre);
	}

	public JComboBox getComboBoxInfraccionLeve() {
		return comboBoxInfraccionLeve;
	}

	public JComboBox getComboBoxInfraccionModerada() {
		return comboBoxInfraccionModerada;
	}

	public JComboBox getComboBoxInfraccionGrave() {
		return comboBoxInfraccionGrave;
	}	
	
}

	