package Vistas;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import json.JsonReaders;
import pojos.RestriccionPerimetral;

import java.awt.Component;
import javax.swing.Box;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JScrollPane;

public class VentanaConfiguracionDeNotificaciones extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnConfigurar;
	private JButton btnCancelar;
	private JTable tablaPersonas;
	private DefaultTableModel modelPersonas;
	private String[] nombreColumnas = {"Apellido","Nombre", "DNI"};
	
	public VentanaConfiguracionDeNotificaciones() 
	{
		super();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 390, 298);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 355, 237);
		contentPane.add(panel);
		panel.setLayout(null);
		
		btnConfigurar = new JButton("Configurar");
//		btnAgregarCamilla.addActionListener(this.controlador);
		btnConfigurar.setBounds(203, 203, 103, 23);
		
		panel.add(btnConfigurar);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		horizontalStrut.setBounds(140, 232, 113, -8);
		panel.add(horizontalStrut);
						
		JLabel lbltitulo = new JLabel("CONFIGURAR SERVICIOS DE NOTIFICACION");
		lbltitulo.setBounds(54, 11, 236, 14);
		panel.add(lbltitulo);
		lbltitulo.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(54, 203, 103, 23);
		panel.add(btnCancelar);
		
		tablaPersonas = new JTable();
		tablaPersonas.setBounds(50, 176, 244, -125);
		panel.add(tablaPersonas);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 31, 335, 166);
		panel.add(scrollPane);
		
		modelPersonas = new DefaultTableModel(null, nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);
		
		//Seteo para los anchos de las columnas
		tablaPersonas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaPersonas.setAutoCreateColumnsFromModel(false);

		int[] anchos = { 100, 100, 100};

		for (int i = 0; i < tablaPersonas.getColumnCount(); i++) {
			tablaPersonas.getColumnModel().getColumn(i).setPreferredWidth(anchos[i]);
			}
		
		scrollPane.setViewportView(tablaPersonas);
	
	}
	
	public JButton getBtnConfigurar() 
	{
		return btnConfigurar;
	}
	
	public JButton getBtnCancelar() 
	{
		return btnCancelar;
	}

	public DefaultTableModel getModelPersonas() {
		return modelPersonas;
	}

	public JTable getTablaPersonas() {
		return tablaPersonas;
	}
	
	

}

	